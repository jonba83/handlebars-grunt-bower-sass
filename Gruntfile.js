module.exports = function(grunt) {
    'use strict';

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        /* SASS task to generate style.css */
        sass: {
          dist: {
            files: {
              'dist/css/style.css' : 'src/sass/style.scss'
            }
          }
        },

        /* minify style.css */
        cssmin: {
          options: {
            sourceMap: true,
            target: 'dist/css',
            keepSpecialComments: 0
          },
          target: {
            files: [{
              expand: true,
              cwd: 'dist/css',
              src: ['style.css'],
              dest: 'dist/css',
              ext: '.min.css'
            }]
          }
        },

        /* beautify generated html by assemble */
        prettify: {
          options: {
            indent: 4,
            indent_char: ' ',
            indent_scripts: 'normal',
            wrap_line_length: 0,
            brace_style: 'collapse',
            preserve_newlines: true,
            max_preserve_newlines: 1,
            unformatted: ['a', 'code', 'pre', 'sub', 'sup', 'b', 'i', 'strong']
          },
          all: {
            expand: true,
            cwd: 'dist',
            src: ['*.html'],
            dest: 'dist',
            ext: '.html',
          }
        },

        /* main.js generation */
        concat: {
          options: {
            separator: ';\n\n',
            sourceMap: true
          },
          dist: {
            src: [
              'src/js/base.js',
              'src/js/base-component.js',
              'src/js/base-select-component.js',
              'src/js/components/**/*.js',
              'src/js/main.js'
            ],
            dest: 'dist/js/main.js',
          },
          vendor: {
            src: [
              'dist/js/vendor/jquery/jquery-1.11.0.min.js',
              'dist/js/vendor/jquery/jquery-placeholder/jquery-placeholder.js',
              'dist/js/vendor/slidebars/slidebars.min.js',
              'dist/js/vendor/polyfills/svgpathseg.polyfill.js',
            ],
            dest: 'dist/js/vendor.js',
          }
        },

        /* uglify and minify javascript files */
        uglify: {
          vendor: {
            files: {
              'dist/js/vendor.min.js': ['dist/js/vendor.js'],
              'dist/js/main.min.js': ['dist/js/main.js']
            }
          }
        },

        /* assemble templating */
        assemble: {
            options: {
                helpers: 'src/structure/helpers/**/*.js',
                layout: 'index.hbs',
                layoutdir: 'src/structure/',
                partials: ['src/structure/partials/**/*.hbs', 'src/content/partials/**/*.hbs'],
                data: 'src/content/data/*.json'
            },
            pages: {
                files: [{
                    expand: true,
                    flatten: true,
                    cwd: 'src/content/pages',
                    dest: 'dist/',
                    src: ['**/*.hbs']
                }]
            }
        },

        /* remove *.html and *.css on dist */
        clean: ['dist/*.html', 'dist/css/*.css'],

       /* watch task */
       watch: {
          css: {
            files: ['src/sass/**/*.scss'],
            tasks: ['sass', 'cssmin']
          },
          js: {
            files: ['src/js/**/*.js'],
            tasks: ['concat']
          },
          html: {
            files: ['src/content/**/*', 'src/structure/**/*'],
            tasks: ['assemble']
          }
        },

      
        /* run server */
        connect: {
          server: {
            options: {
              port: 1337,
              hostname: '0.0.0.0',
              base: 'dist'
            }
          },
          keepaliveserver: {
            options: {
              port: 1337,
              hostname: '0.0.0.0',
              base: 'dist',
              keepalive: true
            }
          }
        }
    });

    /* loading plugins in package.json */
    grunt.loadNpmTasks('assemble');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-prettify');
    grunt.loadNpmTasks('grunt-contrib-connect');

    /* grunt tasks */
    grunt.registerTask('default', ['clean','assemble', 'sass', 'concat', 'concat:vendor', 'uglify:vendor', 'cssmin', 'prettify']);
    grunt.registerTask('dev', ['clean','sass', 'concat', 'concat:vendor', 'uglify:vendor', 'assemble', 'connect:server', 'watch']);

    /* task for only run server */
    grunt.registerTask('server', ['connect:keepaliveserver']);
};