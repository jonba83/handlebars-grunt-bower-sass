var FrontMainController = {

	components: {},
	slidebars: function() {},

	// Creates the list of js components objects
	initComponentsList: function() {
		this.components = [
			ToggleHide,
			PopupTrigger,
			Menu
		]
	},

	// Initialization on document ready
	init: function() {
		this.initComponentsList();
		this.initComponents();
		this.initPlugins();
	},


	// Initialize components for the current view
	initComponents: function() {
		$(this.components).each(function(index, component) {
			component.init();
		});
	},

	// Special initializations for plugins
	initPlugins: function() {
		// Placeholder plugin for IE9 polyfill
		$('input, textarea').placeholder();
		// Slidebars init
		this.slidebars = $.slidebars;
		$.slidebars();
	}
	
}