var Menu = $.extend({/* Menu */}, BaseComponent, {

	mainSearchSelectors: {
		trigger: '#main-search-trigger',
		closer: '#main-search-closer'
	},

	init: function() {

		$(this.mainSearchSelectors.trigger).click(this.showMainSearch.bind(this));
		$(this.mainSearchSelectors.closer).click(this.hideMainSearch.bind(this));
	},

	showMainSearch: function() {
		$('.main-search').show().animate({ height: "80px" }, 200 );
	},

	hideMainSearch: function() {
		$('.main-search').animate({ height: 0 }, 200, function(){ $(this).hide(); });
	}
})