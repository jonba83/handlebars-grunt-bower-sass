var PopupTrigger = $.extend({/* PopupTrigger */}, BaseComponent, {

	componentSelector: '.popup-trigger',
	popupClassSelector: '.popup-info',

	init: function() {
		$(this.componentSelector).each(this.bindingEventsElement.bind(this));
	},

	bindingEventsElement: function(index, element) {
		$(element).click((function(event) {

			// Force close all page popups
			$(this.popupClassSelector).addClass('hide');

			event.stopPropagation();

			if ($(event.currentTarget).next().hasClass('popup-info')) {
				$(event.currentTarget).next().toggleClass('hide');
			} else {
				$(event.currentTarget).parents('.title-with-info, .popup-container').find(this.popupClassSelector).toggleClass('hide');
			}
		}).bind(this));
	}
})