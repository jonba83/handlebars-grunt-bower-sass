var ToggleHide = $.extend({/* ToggleHide */}, BaseComponent, {

	componentSelector: '.toggle-hide',

	init: function() {
		$(this.componentSelector).each(this.bindingEventsElement.bind(this));
	},

	bindingEventsElement: function(index, element) {
		$(element).click((function(event) {
			event.preventDefault();
			var target = $(event.currentTarget).attr('data-toggle-target');
			$(target).toggleClass('hide');
		}).bind(this));

		if ($(element).attr('data-hide-onload')) {
			var target = $(element).attr('data-hide-onload');
			$(target).addClass('hide');
		}
	}
})