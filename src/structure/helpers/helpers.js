module.exports.register = function (Handlebars, options) {
	'use strict';

	Handlebars.registerHelper('replaceStr', function (haystack, needle, replacement) {
		if (haystack && needle) {
			return haystack.replace(needle, replacement);
		} else {
			return '';
		}
	});

	Handlebars.registerHelper('ternary', function(test, yes, no) {
		return test ? yes : no;
	});

	Handlebars.registerHelper('inputAmount', function(value) {
		return value.replace(/\./g, '').replace(/\,/g, '.');
	});

	Handlebars.registerHelper('inputIBAN', function(iban) {
		return iban.replace(/ /g, '');
	});

	Handlebars.registerHelper('matchesStr', function(str1, str2, resultTrue, resultFalse) {
		if (str1 === str2) return resultTrue;
		else return resultFalse;
	});

	Handlebars.registerHelper('setIndex', function(value){
	    this.index = Number(value);
	});

	Handlebars.registerHelper('ifCondition', function (v1, operator, v2, options) {
		switch (operator) {
			case '==':
				return (v1 == v2) ? options.fn(this) : options.inverse(this);
			case '===':
				return (v1 === v2) ? options.fn(this) : options.inverse(this);
			case '!=':
				return (v1 != v2) ? options.fn(this) : options.inverse(this);
			case '!==':
				return (v1 !== v2) ? options.fn(this) : options.inverse(this);
			case '<':
				return (v1 < v2) ? options.fn(this) : options.inverse(this);
			case '<=':
				return (v1 <= v2) ? options.fn(this) : options.inverse(this);
			case '>':
				return (v1 > v2) ? options.fn(this) : options.inverse(this);
			case '>=':
				return (v1 >= v2) ? options.fn(this) : options.inverse(this);
			case '&&':
				return (v1 && v2) ? options.fn(this) : options.inverse(this);
			case '||':
				return (v1 || v2) ? options.fn(this) : options.inverse(this);
			default:
				return options.inverse(this);
		}
	});

	Handlebars.registerHelper('math', function(lvalue, operator, rvalue, options) {
		lvalue = parseFloat(lvalue);
		rvalue = parseFloat(rvalue);

		return {
			'+': lvalue + rvalue,
			'-': lvalue - rvalue,
			'*': lvalue * rvalue,
			'/': lvalue / rvalue,
			'%': lvalue % rvalue
		}[operator];
	});

	Handlebars.registerHelper('getDataJSON', function(value) {
		return JSON.stringify(value);
	});
};