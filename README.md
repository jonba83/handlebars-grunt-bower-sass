Handlebars & Grunt Boilerplate Project README
##########################################


Initial Grunt Configuration (SASS and JS compiling)
---------------------------

1. Install Node.js to use npm package manager (https://nodejs.org/download/)

2. Execute the following command (with sudo for OSX / Linux or run your command shell as Administrator for Windows. more info: http://gruntjs.com/getting-started#installing-the-cli)

		> npm install -g grunt-cli

3. Once installed grunt-cli globally, execute the following command to install necessary modules

		> npm install

4. To run the fifth step you must to have Ruby (https://www.ruby-lang.org/en/downloads/) and Sass installed.

	Note: If you're on OS X or Linux you probably already have Ruby installed; test with ruby -v in your terminal.

	> gem install sass (to install Sass)

5. Finally run grunt tasks defined on Gruntfile.js

	(runs watch task to update sass or files changes)

		> grunt dev

	(default grunt task, generate style.css and main.js without looking for changes)

		> grunt